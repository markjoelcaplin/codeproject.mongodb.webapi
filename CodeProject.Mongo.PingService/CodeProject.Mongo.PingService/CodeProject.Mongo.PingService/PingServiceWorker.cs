﻿using CodeProject.Mongo.Business.Service;
using CodeProject.Mongo.Data.MongoDb;
using CodeProject.Mongo.Data.Transformations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace CodeProject.Mongo.PingService
{
	public class PingServiceWorker
	{
		public void ExecutePingService()
		{
			Timer tm = null;

			tm = new Timer(300000);
			tm.Elapsed += new ElapsedEventHandler(Timer_Elapsed);
			tm.Start();
			

		}

		protected void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			OnlineStoreDataService onlineStoreDataService = new OnlineStoreDataService();
			OnlineStoreBusinessService onlineStoreBusinessService = new OnlineStoreBusinessService(onlineStoreDataService);

			UserDataTransformation user = new UserDataTransformation();
			user.EmailAddress = "markcap@hotmail.com";
			user.Password = "test";

			onlineStoreBusinessService.Login(user).Wait();
			Console.WriteLine("Ping service executed at ..." + DateTime.Now.ToString());
		}
	}

	

}
