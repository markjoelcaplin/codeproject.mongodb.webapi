﻿using System;
using CodeProject.Mongo.Business.Service;
using CodeProject.Mongo.Data.Models.Configuration;
using CodeProject.Mongo.Data.Models.Entities;
using CodeProject.Mongo.Data.MongoDb;
using CodeProject.Mongo.Data.Transformations;
using Microsoft.Extensions.PlatformAbstractions;
using MongoDB.Driver;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Timers;

namespace CodeProject.Mongo.PingService
{
	class Program
	{
		static void Main(string[] args)
		{

			PingServiceWorker worker = new PingServiceWorker();
			worker.ExecutePingService();

			Console.WriteLine("Press any key to stop...");
			Console.ReadKey();


		}

	}
		
}
